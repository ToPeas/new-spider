// const http = require('http') const server = http.createServer((req, res) => {
//   console.log(req)   const url = req.url   res.writeHead(200,
// {'Content-Type': 'text/plain;charset=utf-8'})   res.end('你访问的地址是' + url) })
// console.log(Object.prototype.toString.call(server)) //
// console.log(server.prototype) server.listen(3000, (err) => {   if (err)
// console.log(err)   console.log('服务启动在3000端口') })

const http = require('http')
const fs = require('fs')
let options = {
  hostname: 'www.example.com',
  port: 80,
  path: '/',
  method: 'GET'
}

const req = http.request(options, (res) => {
  console.log(`STATUS: ${res.statusCode}`) //返回状态码
  let out = fs.createWriteStream('./log.txt')
  out.write(JSON.stringify(res.headers))
  console.log(`HEADERS: ${JSON.stringify(res.headers, null, 4)}`) // 返回头部
  res.setEncoding('utf8') // 设置编码
  res.on('data', (chunk) => { //监听 'data' 事件
    console.log(`主体: ${chunk} =>>`)
  })

})

req.end() // end方法结束请求

// const http = require('http') //引入http模块 const fs = require('fs') //引入fs模块 文件
// I/O const hostname = '127.0.0.1' const port = 3000 const server =
// http.createServer((req, res) => { //创建一个http服务器   res.statusCode = 200
// res.setHeader('Content-Type', 'text/plain')   if(req.url !== '/favicon.ico'){
//       let out = fs.createWriteStream('./log.txt') // 创建写入流
// out.write(`请求方法：${req.method} \n`)       out.write(`请求url：${req.url} \n`)
// out.write(`请求头对象：${JSON.stringify(req.headers, null, 4)} \n`)
// out.write(`请求http版本：${req.httpVersion} \n`)   }   res.end('Hello World\n') })
// server.listen(port, hostname, () => {   console.log(`服务器运行在
// http://${hostname}:${port}`) })
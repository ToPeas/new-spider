// const fs = require('fs-extra')

const fs = require('fs')

const zlib = require('zlib')

const cb = (err, res) => {
  if (err) console.log(err)
  console.log(res)
}
// fs.mkdir('./tmpc', (err, res) => {
//   if (err) console.log(err)
//   console.log(res)
// })

// 查看一个文件夹
// fs.readdir('./node_modules', cb)

// 查看一个文件的详细信息
// fs.stat('./yarn.lock', cb)

// fs.open('./app.js', 'r', (err, res) => {
//   if (err) console.log(err)
//   fs.fstat(res, cb)
// })

//  查看一个文件夹是否存在
// fs.exists('./app.js',cb)

// 修改文件的访问时间和修改时间
// fs.utimes

// 修改文件和文件夹的权限
// fs.chmodSync


// fs的流处理
// fs.createReadStream('./picture/落地页.png').pipe(zlib.createGzip()).pipe(fs.createWriteStream('./落地页.png.gz'))


// setTimeout(() => {
//   fs.createReadStream('./落地页.png.gz').pipe(zlib.createGunzip()).pipe(fs.createWriteStream('./转换成的.png'))
// }, 1000);


// 检查用户的权限
// fs.access('./app.js', cb)
// 打开文件，有标记符'r','w'
// fs.open()

// const path = require('path')

// console.log(__dirname)
// console.log(__filename)

// const stream = fs.createReadStream('./app.js')
// let data = ''
// stream.on('data', (chunk) => {
//   data += chunk
// })

// stream.on('end', () => {
//   console.log(data)
// })

// 按行读取文件
// const line = require('readline')
// const rl = line.createInterface({
//   input: fs.createReadStream('./app.js')
// })
// rl.on('line', (line) => {
//   console.log('=>', line);
//   console.log('-------------')
// });


// 写数据进一个文件
// const write = fs.createWriteStream('./a.txt', {
//   encoding: 'utf-8'
// })
// write.write('asydfgauihodj;kd')
// write.end(cb)

// 有交互的
// const readline = require('readline');

// const rl = readline.createInterface({
//   input: process.stdin,
//   output: process.stdout
// });

// rl.question('你认为 Node.js 中文网怎么样？', (answer) => {
//   // 对答案进行处理
//   console.log(`多谢你的反馈：${answer}`);

//   rl.close();
// });

fs.unlink('./a.txt', cb)
const superagent = require('superagent')
require('superagent-proxy')(superagent);

// 写上你先要测试的 ip，下面仅为测试ip
let testIp = 'http://61.178.238.122:63000';

(async function () {
  superagent.get('http://ip.chinaz.com/getip.aspx').proxy(testIp).timeout(3000)
    .end((err, res) => {
      if (res === undefined) {
        console.log('挂了');
        return
      }
      if (err) {
        console.log('报错啦')
      }
      console.log('成功： ' + res.text)
    })
}())
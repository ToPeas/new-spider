const fs = require('fs')

const out = process.stdout

const src = './1.mp4'
const dist = './2.mp4'


function cc() {
  const read1 = fs.createReadStream(src)
  const write1 = fs.createWriteStream(dist)

  let stas = fs.statSync(src)
  console.log(process.cwd())
  let totalSize = stas.size
  let lastSize = 0
  let progress = 0
  process.exit(0)
  // read1.pipe(write1)

  console.time('  持续时间')
  read1.on('data', (chunk) => {
    // console.log(chunk.length)
    progress += chunk.length
  })

  write1.on('finish', () => [
    console.log('移动成功')
  ])
  setTimeout(function show() {
    let percent = Math.ceil((progress / totalSize) * 100)
    let size = Math.ceil(progress / 1000000)
    let diff = size - lastSize
    lastSize = size
    out.clearLine()
    out.cursorTo(0)
    out.write(`已完成${size}MB,${percent}%, 速度：${diff * 2}MB/s`)
    if (progress < totalSize) {
      setTimeout(show, 100)
    } else {
      let endTime = Date.now()
      console.timeEnd('  持续时间')
      // console.log(`共用时：${(endTime - startTime) / 1000}秒。`)
    }
  }, 100);
}

cc()
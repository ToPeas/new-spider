// const cluster = require('cluster')
// const cpuNums = require('os').cpus().length;

// console.log('电脑的cpu数量', cpuNums)


// if (cluster.isMaster) {
//   for (let i = 0; i < cpuNums; i++) {
//     let worker = cluster.fork()
//     worker.send(`this messgae is from master, i am son ${i}`)
//   }
// }

// process.on('message', (msg) => {
//   console.log(msg)
//   console.log(`i have received your message , my pid is ${process.pid}`)
// })
const fs = require('fs')
const axios = require('axios')
const cheerio = require('cheerio')
const path = require('path')


let url = 'http://www.qyl72.com'

const linkArr = []
let name = ''

const getUrl = async (page = 1) => {

  let maxPage = 0
  let currentPage = 1
  const res = await axios({
    method: 'get',
    url
  })
  const $ = cheerio.load(res.data)

  if (page === 1) {
    name = $('.article h2').text()
  }
  if (!maxPage) {
    maxPage = $('#page i + a').text()
  }
  if (currentPage < maxPage) {
    const link = $('#content img').attr('data-img')
    linkArr.push(link)
    getUrl(page + 1)
  } else {
    console.log('爬完了')
    console.log(name)
    for (let i of linkArr)
      download(name, i)
  }
  // $('.videos .video img').each((index, item) => {
  //   linkArr.push({
  //     link: item.attribs.src,
  //     name: item.attribs.alt,
  //   })
  // })
  // linkArr.forEach(item => {
  //   download(item.name, item.link)
  // })
}

const download = async (name, link) => {
  console.log(`正在下载${name}`)
  const filename = name + '.' + link.split('.')[3]
  await axios({
    url: link,
    method: 'get',
    headers: {
      'Referer': 'http://www.qyl72.com/'
    },
    responseType: 'stream'
  }).then(res => {
    return res.data.pipe(fs.createWriteStream(path.join(__dirname, 'mm', filename)))
  })
}


getUrl()
// console.log(getUrl().then(res => res))